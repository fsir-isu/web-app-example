<?php

namespace App\Models;

class Order extends BaseModel {

    public $username;
    public $telephone;
    public $subject;
    public $spam = false;

    protected static $subjects = [
        'Бизнес',
        'Технологии',
        'Веб-разработка',
        'Реклама и Маркетинг',
    ];

    protected static $table = 'orders';

    protected static $attributes = [
        'username',
        'telephone',
        'subject',
        'spam',
    ];


    public static function list_subjects()
    {
        return static::$subjects;
    }

    public function validate()
    {
        if (empty($this->username))
        {
            $this->errors['username'] = 'Не введено имя';
        }

        if (empty($this->telephone))
        {
            $this->errors['telephone'] = 'Не введен телефон';
        }

        if (empty($this->subject))
        {
            $this->errors['subject'] = 'Не выбрана тема';
        }

        return ! $this->has_errors();
    }

}

