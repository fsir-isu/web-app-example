<?php

namespace App;

use PDO;

class Database {

    protected static $_pdo;

    public static function get_pdo()
    {
        if (empty(static::$_pdo))
        {
            $config = Config::get('database', [
                'host'   => 'localhost',
                'dbname' => 'test',
                'user'   => 'root',
                'passw'  => '',
            ]);

            static::$_pdo = new PDO('mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'],
                $config['user'], 
                $config['passw']);
        }

        return static::$_pdo;
    }

}