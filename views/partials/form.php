
<div class="form-group" v-bind:class="{'has-error': !!errors.username}">
    <label>ФИО:</label>
    <input class="form-control" type="text" v-model="order.username">
    <span class="help-block" v-cloak v-if="errors.username">{{ errors.username }}</span>
</div>
<div class="form-group" v-bind:class="{'has-error': !!errors.telephone}">
    <label>Телефон:</label>
    <input class="form-control" type="text" v-model="order.telephone">
    <span class="help-block" v-cloak v-if="errors.telephone">{{ errors.telephone }}</span>
</div>
<div class="form-group" v-bind:class="{'has-error': !!errors.subject}">
    <label>Тема:</label>
    <select class="form-control" v-model="order.subject">
        <option v-for="subject in subjects" v-bind:value="subject">{{ subject }}</option>
    </select>
    <span class="help-block" v-cloak v-if="errors.subject">{{ errors.subject }}</span>
</div>

<div class="form-group">
    <div class="checkbox">
        <label>
            <input type="checkbox" v-model="order.spam" v-bind:true-value="1" v-bind:false-value="0"> Согласен на получение рассылки
        </label>
    </div>
</div>

<div class="form-group">
    <button class="btn btn-primary" type="submit" v-bind:disabled="sending">Отправить</button>
    <span class="text-info" v-cloak v-if="sending">Отправляю...</span>
    <span class="text-danger" v-cloak v-if="danger_message">{{ danger_message }}</span>

</div>
